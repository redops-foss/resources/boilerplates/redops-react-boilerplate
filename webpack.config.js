const chalk = require('chalk');
const path = require('path');

const CONSTANTS = require('./.webpack/constants');

module.exports = (
  { bundleAnalysis } = {},
  { mode = CONSTANTS.DEVELOPMENT.MODE } = {},
) => {
  const shouldGenerateReport = bundleAnalysis === 'true';

  /* eslint-disable no-console */
  console.log(chalk`Building for {cyan.bold ${mode}}`);
  console.log(`• Generate bundle analysis report: ${shouldGenerateReport ? chalk.green.bold('✔') : chalk.red.bold('❌')}`);
  /* eslint-enable no-console */

  const publicPath = '/';

  return {
    mode,

    entry: [
      './src/index.js',
    ],

    output: {
      path: path.join(__dirname, CONSTANTS.DEVELOPMENT.CONTENT_BASE),
      filename: 'js/bundle.min.js',
      chunkFilename: 'js/chunks/[name].chunk.js',
      publicPath,
    },

    devServer: mode === CONSTANTS.DEVELOPMENT.MODE
      ? {
        publicPath,
        contentBase: CONSTANTS.DEVELOPMENT.CONTENT_BASE,
        watchContentBase: true,
        watchOptions: {
          ignored: /node_modules/,
        },
        stats: 'minimal',
        hot: true,
        transportMode: 'ws',
        injectClient: false,
        proxy: {
          [process.env.REVERSE_PROXY_PATH || '/api']: process.env.REVERSE_PROXY_TARGET_URL,
        },
        historyApiFallback: {
          disableDotRule: true,
        },
        port: +(process.env.LOCAL_APP_PORT || 8080),
      }
      : undefined
  };
};
